window.addEventListener("load", () => {
	let body = document.body;
	let style = document.createElement('style');
	let sheet;

	const LS_NAME = "vylda-clickable";
	const ANCHOR_CLASS_NAME = "anchored-by-vylda";

	let ls = window.localStorage;

	let setupObjInit = {
		anchors: false,
		maps: true
	};

	let saved = ls.getItem(LS_NAME);

	let setup = Object.assign({}, setupObjInit, saved ? JSON.parse(saved) : {});



	let dom = {
		panel: document.createElement("div"),
		showAnchors: document.createElement("label"),
		showMaps: document.createElement("label"),
		showAnchorsSw: document.createElement("input"),
		showMapsSw: document.createElement("input"),
		icon: new Image(20, 20)
	}

	console.log(setup);

	dom.icon.src = "http://www.gcgpx.cz/clickablaearea/clickablaearea.png";
	dom.panel.appendChild(dom.icon);

	dom.icon.addEventListener("click", () => {
		dom.panel.classList[dom.panel.classList.contains("open") ? "remove" : "add"]("open");
	});

	dom.showAnchorsSw.addEventListener("change", e => {

		let anchors = document.querySelectorAll("a");
		Array.from(anchors).forEach(a => {
			let cs = window.getComputedStyle(a);
			if (cs.display == "inline") {
				a.style.display = "inline-block";
			};
			a.classList[e.target.checked ? "add" : "remove"](ANCHOR_CLASS_NAME);
			saveLs("anchors", e.target.checked)
		});
	});
	dom.showMapsSw.addEventListener("change", e => {
		saveLs("maps", e.target.checked)
	});

	dom.showAnchors.textContent = "anchors";
	dom.showAnchorsSw.type = "checkbox";
	dom.showAnchorsSw.checked = setup.anchors;

	dom.showAnchors.appendChild(dom.showAnchorsSw);
	dom.showMaps.textContent = "image maps";
	dom.showMapsSw.type = "checkbox";
	dom.showMapsSw.checked = setup.maps;
	dom.showMaps.appendChild(dom.showMapsSw);

	dom.panel.appendChild(dom.showAnchors);
	dom.panel.appendChild(dom.showMaps);

	dom.panel.id = "vylda-clickable-area";

	if (!body.querySelector("vylda-clickable-area")) {
		body.appendChild(dom.panel);
		document.head.appendChild(style);
		mysheet = style.sheet;

		let styles = '#vylda-clickable-area {';
		styles += 'position: absolute;';
		styles += 'top: 5px;';
		styles += 'left: 5px;';
		styles += 'z-index: 999;';
		styles += 'padding: 5px;';
		styles += 'background-color: rgba(0, 0, 0, 0.5);';
		styles += 'color: white;';
		styles += 'font-family: sans-serif;';
		styles += 'max-width: 17px;';
		styles += 'max-height: 17px;';
		styles += 'overflow: hidden;';
		styles += 'transition: all 0.5s;';
		styles += '}';
		mysheet.insertRule(styles, 0);

		styles = '#vylda-clickable-area.open {';
		styles += 'max-width: 100px;';
		styles += 'max-height: 70px;';
		styles += '}';
		mysheet.insertRule(styles, 0);

		styles = '#vylda-clickable-area img {';
		styles += 'cursor: pointer;';
		styles += '}';
		mysheet.insertRule(styles, 0);

		styles = '#vylda-clickable-area label {';
		styles += 'display: block;';
		styles += 'font-size: 12px;';
		styles += '}';
		mysheet.insertRule(styles, 0);

		styles = '#vylda-clickable-area label input {';
		styles += 'position: relative;';
		styles += 'top: 2px;';
		styles += '}';
		mysheet.insertRule(styles, 0);

		styles = '.anchored-by-vylda {';
		styles += "padding: 2px;";
		styles += "background: rgba(123, 249, 155, 0.75);";
		styles += "border: 1px solid rgb(46, 58, 191);";
		styles += "line-height: 1;";
		styles += "cursor: pointer;";
		styles += '}';
		mysheet.insertRule(styles, 0);
	}

	let anchors = document.querySelectorAll("a");
	Array.from(anchors).forEach(a => {
		let cs = window.getComputedStyle(a);
		if (cs.display == "inline") {
			a.style.display = "inline-block";
		};
		if (setup.anchors) {
			a.classList.add(ANCHOR_CLASS_NAME);
		}
	});
});
window.addEventListener("load", () => {
	mapAreaStart();
});

function mapAreaStart() {
	let maps = document.querySelectorAll("map");
	Array.from(maps).forEach(function(map) {
		showMapArea(map);
	});
}

function showMapArea(map) {
	if (!map) {
		return;
	}
	let mapId =  map.name || map.id;
	let images = document.querySelectorAll("img[usemap=\"#" + mapId + "\"]");

	Array.from(images).forEach(function(image) {
		let style = window.getComputedStyle(image);
		let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		svg.style.width = style.width;
		svg.style.height = style.height;
		svg.style.position = "absolute";
		svg.style.top = "0px";
		svg.style.left = "0px";
		svg.style.pointerEvents =  "none";

		let wrapper = document.createElement("div");
		wrapper.style.position = "relative";
		wrapper.style.width = style.width;
		wrapper.style.height = style.height;
		wrapper.style.display = "inline-block";
		image.parentNode.insertBefore(wrapper, image);
		wrapper.appendChild(image);
		wrapper.appendChild(svg);


		let areas = map.querySelectorAll("area");
		Array.from(areas).forEach(function(area) {
			let coords = area.getAttribute("coords");
			let shape = area.getAttribute("shape");
			if (coords && shape) {
				coords.replace(/\s+/g, "");
				let coordArr = coords.split(",");

				switch (shape.toLowerCase()) {
					case "rectangle":
					case "rect":
						if(Array.isArray(coordArr) && coordArr.length === 4) {
							let rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
							let x = Math.min(coordArr[0], coordArr[2]);
							let w = Math.max(coordArr[0], coordArr[2]) - x;
							let y = Math.min(coordArr[1], coordArr[3]);
							let h = Math.max(coordArr[1], coordArr[3]) - y;
							rect.setAttributeNS(null, "x", x);
							rect.setAttributeNS(null, "y", y);
							rect.setAttributeNS(null, "width", w);
							rect.setAttributeNS(null, "height", h);
							setBaseSvgAttribs(rect);
							svg.appendChild(rect);
						}
						break;
					case "circle":
					case "circ":
						if(Array.isArray(coordArr) && coordArr.length === 3) {
							let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
							let x = coordArr[0];
							let y = coordArr[1];
							let r = coordArr[2];
							circle.setAttributeNS(null, "cx", x);
							circle.setAttributeNS(null, "cy", y);
							circle.setAttributeNS(null, "r", r);
							setBaseSvgAttribs(circle);
							svg.appendChild(circle);
						}
						break;
					case "polygon":
					case "poly":
						if(Array.isArray(coordArr) && coordArr.length % 2 === 0) {
							let poly = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
							let points  = new Array();
							for (let p = 0; p < coordArr.length; p += 2) {
								points.push(coordArr[p] + "," + coordArr[p + 1]);
							}

							poly.setAttributeNS(null, "points", points.join(" "));
							setBaseSvgAttribs(poly);
							svg.appendChild(poly);
						}
						break;
					default:
						break;
				}

			}
		});
	});


}

function setBaseSvgAttribs(obj) {
	obj.setAttributeNS(null, "stroke", "red");
	obj.setAttributeNS(null, "stroke-width", "1");
	obj.setAttributeNS(null, "fill", "#dcf01f");
	obj.setAttributeNS(null, "fill-opacity", "0.4");
}

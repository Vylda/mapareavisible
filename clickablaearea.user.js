// ==UserScript==
// @name		Clickable Areas
// @namespace	http://gcgpx.cz/clickablaearea/
//@include		http*://*
// @version		2.1
// @description	Show clickable areas on image
// @updateURL	http://www.gcgpx.cz/clickablaearea/clickablaearea.meta.js
// @downloadURL http://www.gcgpx.cz/clickablaearea/clickablaearea.user.js
// @icon 		http://www.gcgpx.cz/clickablaearea/clickablaearea.png
// @icon64 		http://www.gcgpx.cz/clickablaearea/clickablaearea64.png
// @grant		none
// @license		GNU General Public License v2.0
// @copyright	2018-2050 Vilem Lipold (Vylda)
// @author		Vilem Lipold (Vylda)
// @run-at		document-idle
// ==/UserScript==

/*
testovací stránky:
- http://www.gcgpx.cz/clickablaearea/
- https://geology.com/state-map/
- https://geology.com/world/
- http://home.chpc.utah.edu/~davidr/tut/tut23_ex/test.html
- https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_areamap
- https://www.sporcle.com/games/captainchomp/click-the-west-virginia-counties-map
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/area#Result
- http://docs.alfresco.com/adg/concepts/world-map.html
- https://en.wikiversity.org/wiki/Image_Map/Tutorial/Clickable_Areas
- http://graphicdesign.spokanefalls.edu/tutorials/tech/imagemapexample/imagemap.htm
*/

(function () {

	const win = window.wrappedJSObject || window;
	const style = document.createElement('style');

	const isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

	const LS_NAME = 'vylda-clickable';
	const ANCHOR_CLASS_NAME = 'anchored-by-vylda';
	const SVG_CLASS_NAME = 'mapped-by-vylda';

	const ls = win.localStorage;

	const setupObjInit = {
		anchors: false,
		maps: true
	};

	let saved = null;
	if (ls) {
		saved = ls.getItem(LS_NAME);
	}

	const setup = { ...setupObjInit, ...(saved ? JSON.parse(saved) : {}) };

	const dom = {
		panel: document.createElement('div'),
		showAnchors: document.createElement('label'),
		showMaps: document.createElement('label'),
		showAnchorsSpan: document.createElement('span'),
		showMapsSpan: document.createElement('span'),
		showAnchorsSw: document.createElement('input'),
		showMapsSw: document.createElement('input'),
		icon: new Image(17, 17)
	}

	const setBaseSvgAttribs = (obj) => {
		obj.setAttributeNS(null, 'stroke', 'red');
		obj.setAttributeNS(null, 'stroke-width', '2');
		obj.setAttributeNS(null, 'fill', '#dcf01f');
		obj.setAttributeNS(null, 'fill-opacity', '0.4');
	}

	const saveLs = (what, value) => {
		setup[what] = value;
		if (ls) {
			if (setup.anchors == false && setup.maps == true) {
				ls.removeItem(LS_NAME);
			} else {
				ls.setItem(LS_NAME, JSON.stringify(setup));
			}
		}
	}


	const showMapArea = (map) => {
		if (!map) {
			return;
		}

		if (map.name) {
			map.name = map.name.toLowerCase()
		};

		if (map.id) {
			map.id = map.id.toLowerCase()
		};

		const mapId = map.name || map.id;
		const images = Array.from(document.querySelectorAll('img[usemap]')).filter((img) => {
			const imgMap = img.getAttribute('usemap');
			if (imgMap) {
				img.setAttribute('usemap', imgMap.toLowerCase());
				return imgMap.toLowerCase() == '#' + mapId;
			}
			return false;
		});


		images.forEach((image) => {
			const computedStyle = window.getComputedStyle(image);
			const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
			svg.classList.add(SVG_CLASS_NAME);
			svg.style.width = '100%';
			svg.style.height = '100%';
			svg.style.position = 'absolute';
			const topMargin = parseFloat(computedStyle.marginTop) || 0;
			const topBorder = isChrome ? 0 : parseFloat(computedStyle.borderTopWidth) || 0;
			const leftMargin = parseFloat(computedStyle.marginLeft) || 0;
			const leftBorder = isChrome ? 0 : parseFloat(computedStyle.borderLeftWidth) || 0;
			svg.style.top = (topMargin + topBorder) + 'px';
			svg.style.left = (leftMargin + leftBorder) + 'px';
			svg.style.pointerEvents = 'none';

			const wrapper = document.createElement('div');
			wrapper.style.position = 'relative';
			wrapper.style.width = computedStyle.width;
			wrapper.style.height = computedStyle.height;
			wrapper.style.display = 'inline-block';
			image.parentNode.insertBefore(wrapper, image);
			wrapper.appendChild(image);
			wrapper.appendChild(svg);


			const areas = map.querySelectorAll('area');
			Array.from(areas).forEach(function (area) {
				const coords = area.getAttribute('coords');
				let shape = area.getAttribute('shape');
				if (coords) {
					coords.replace(/\s+/g, '');
					const coordArr = coords.split(',');
					if (!Array.isArray(coordArr)) {
						return;
					}
					if (!shape) {
						switch (coordArr.length) {
							case 4:
								shape = 'rectangle';
								break;
							case 3:
								shape = 'circle';
								break;
							default:
								if (coordArr.length % 2 === 0) {
									shape = 'polygon';
								}
						}
					}

					switch (shape?.toLowerCase()) {
						case 'rectangle':
						case 'rect':
							if (coordArr.length === 4) {
								const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
								const x = Math.min(coordArr[0], coordArr[2]);
								const w = Math.max(coordArr[0], coordArr[2]) - x;
								const y = Math.min(coordArr[1], coordArr[3]);
								const h = Math.max(coordArr[1], coordArr[3]) - y;
								rect.setAttributeNS(null, 'x', x);
								rect.setAttributeNS(null, 'y', y);
								rect.setAttributeNS(null, 'width', w);
								rect.setAttributeNS(null, 'height', h);
								setBaseSvgAttribs(rect);
								svg.appendChild(rect);
							}
							break;
						case 'circle':
						case 'circ':
							if (coordArr.length === 3) {
								const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
								const x = coordArr[0];
								const y = coordArr[1];
								const r = coordArr[2];
								circle.setAttributeNS(null, 'cx', x);
								circle.setAttributeNS(null, 'cy', y);
								circle.setAttributeNS(null, 'r', r);
								setBaseSvgAttribs(circle);
								svg.appendChild(circle);
							}
							break;
						case 'polygon':
						case 'poly':
							if (coordArr.length % 2 === 0) {
								const poly = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
								const points = new Array();
								for (let p = 0; p < coordArr.length; p += 2) {
									points.push(coordArr[p] + ',' + coordArr[p + 1]);
								}

								poly.setAttributeNS(null, 'points', points.join(' '));
								setBaseSvgAttribs(poly);
								svg.appendChild(poly);
							}
							break;
						default:
							console.warn(`Souřadnice ${coordArr.toString()} se nepodařilo zpracovat`);
							break;
					}

				}
			});
		});


	}

	const mapAreaStart = () => {
		const maps = document.querySelectorAll('map');
		console.info(`Clickable Area: nalezeno ${maps.length} klikacích map ((${location.href}).`);
		Array.from(maps).forEach(function (map) {
			showMapArea(map);
		});
	}

	const buildMenu = () => {
		if (!document.body.querySelector('vylda-clickable-area')) {
			dom.icon.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAWJAAAFiQFtaJ36AAAE3UlEQVRYhb2VbWhbVRjH/2lzm6Qvt+mu2Kx0pBlb0bX2XopNXcdsNlihK8xIPgjKtFAmDGVkxa/CwC+CKJn4hs41mx/sPggT/aCuSMf2ZazaVGi3dWO5kOqSjfTldksWb9Irz0lzl3S9SdaqDxw499zz8nv+z/OcY9KEWgXOpzL4P+yuEkMk/kz+SWbt1Z557ePDTqPjk4kKRGO1SCY4cFxl3riKLUIKjY4kOE4ri75i5ztLa8fMRpNV1YSbs/UwwQaXqwa26srH5ihKGrdmH8DMJbCzVdmQhOsCKAoH+bYdru08eN6Qkf3b1VaPeNyG3ybM6BAXylbDECDruR2i2ACOM0FVNYTDD5BWNZg5kz4vmViBIHBo3maDIFSB4xrw+wTQvXt+cwB/TDXoh5PEM9PLEEV+3RDE439jYmIRoljP1NjZWo+bs+knCkdF4YZWOBpr2eHJRAbh2wm8sLtBP5wOpEZgZOQ5HT41taR/JxM2lrgbAohFa5ikZNMzy9jVVsf6dOBUSGHhYN9LKvOcxgm2tbUWs7P32b/W1jpEY3VlAxSEIK1mP8l7kjRfCVHiH00UgEaHNRseKZuoc5EsHKn1MGGcuIYKKEolBMHC+iSzw2Fl/XA4oSuRbwTX6LBgLpJkowSRCw3weL6UBKDsz8U6kczo5UeZz+VlvyzHIct3WN/hsLC5Oc9VdWV1DVc2QJGLSGMHp9XCuj4T/AmyvAC73YI9e9rQIXass3qlbABdAZ7PMOmxms25PikRjab0Bb2eDni9LyMQ+BIc58Rnn36H4/4vcPnSDfB81vO0Wv7TogPQDZZMpHSAublsbKkq5lfLj8zjeQ6h0CTre71enDz5CU6c+BCyrODNIwEEAt9DWV4sG6AgBLZqlWU9xZNBRJIMgJKQkjEWTbGcuHs3WrCJ3W6H3+9n/VAohJGRUzg3uoxeTxNe8raVD9DiUnBt2gpRssPlqmZlRvJTstF3zqqq0gbbAZIkMVXIzp8/j2H/zwDmccz/IravM7/gIqIwbBGW9dIiz5PJDAN5VGKAKO1gnpYyClEg8Dm83qPY7/kAr1XWNRZVACzmCcxMLwGRbPzJc6qIWPQhotGHbI7T2YLx8XHmrZEFAgHI8gwVNURpK34d92P7gfdjJQGyni9hLpLBVKiGPbdUjrkrGuy63YGzZ34s6r8s38DrgzsgSU1F5xneA83b7kMQUpiZToHjLOB5S8GLaKu2GS1lRtL7/UeBQRSFKPpsUVV0SIt4ti2Guvo70HBPb66WasiyXBLibDCMUOivjQHkG8+rEIT7eutyP60nIoFQxm8EovyHe431eigRL7DBYPA0gsGPSkJcNVsta/+ZtOYt12HlrBuBePf53q3vfXuu6vB+T/qbyPU/37I3NR756rTVqDreHnwjdfXa9QNXrly5pA9qmrbhtm/fvovhcFjr7+//OreHz+ebmZyc1IxsaGjontvt3pubvymAgwcPHh8YGKBHokXfELC3t7dHy4XYFAAd7HQ64+uM2/v6+m4tLCyUhNgUALVDhw51GMDZfT5f3AiCFOrs7Pxh0wCl4Nxu9/JaiJGREU0UxcubDkE5jWQmJfIP7+np+SW31rQq139q3d3de7u6ui5IkrQyNjZ2anR09Ni/UoZP0oaHh1+hqilYo2n4Bx3yS31pPsS8AAAAAElFTkSuQmCC';
			dom.icon.title = 'Clickable Areas setup menu';
			dom.panel.appendChild(dom.icon);

			dom.icon.addEventListener('click', () => {
				dom.panel.classList[dom.panel.classList.contains('open') ? 'remove' : 'add']('open');
			});

			dom.showAnchorsSw.addEventListener('change', e => {
				saveLs('anchors', e.target.checked);
				anchorsStart();
			});
			dom.showMapsSw.addEventListener('change', e => {
				saveLs('maps', e.target.checked);
				const svgs = document.querySelectorAll(`svg.${SVG_CLASS_NAME}`);
				Array.from(svgs).forEach(svg => {
					svg.remove();
				})
				if (setup.maps) {
					mapAreaStart();
				}
			});

			dom.showAnchorsSpan.textContent = 'anchors';
			dom.showAnchorsSw.type = 'checkbox';
			dom.showAnchorsSw.checked = setup.anchors;
			dom.showAnchors.appendChild(dom.showAnchorsSpan);
			dom.showAnchors.appendChild(dom.showAnchorsSw);
			dom.showAnchors.title = 'Show all anchors (<a>) tags.';

			dom.showMapsSpan.textContent = 'image maps';
			dom.showMapsSw.type = 'checkbox';
			dom.showMapsSw.checked = setup.maps;
			dom.showMaps.appendChild(dom.showMapsSpan);
			dom.showMaps.appendChild(dom.showMapsSw);
			dom.showMaps.title = 'Show all clickable areas on images.';

			dom.panel.appendChild(dom.showAnchors);
			dom.panel.appendChild(dom.showMaps);

			dom.panel.id = 'vylda-clickable-area';

			document.head.appendChild(style);
			const mysheet = style.sheet;

			let styles = `#vylda-clickable-area {
				position: fixed;
				top: 5px;
				left: 5px;
				z-index: 99999999;
				display: block;
				padding: 5px;
				background-color: rgba(0, 0, 0, 0.5);
				color: white;
				font-family: sans-serif;
				max-width: 15px;
				max-height: 15px;
				overflow: hidden;
				transition: all 0.5s;
				box-sizing: content-box;
			}`;
			mysheet.insertRule(styles, 0);

			styles = `#vylda-clickable-area.open {
				max-width: 100px;
				max-height: 70px;
			}`;
			mysheet.insertRule(styles, 0);

			styles = `#vylda-clickable-area img {
				cursor: pointer;
				position: static;
			}`;
			mysheet.insertRule(styles, 0);

			styles = `#vylda-clickable-area label {
				display: block;
				font-size: 12px;
				text-transform: none;
				font-weight: normal;
				font-style: normal;
				margin: 0;
				line-height: 1.5;
				white-space: nowrap;
			}`;
			mysheet.insertRule(styles, 0);

			styles = `#vylda-clickable-area label span {
				display: inline-block;
				margin: 0;
				padding: 0;
				font-size: 12px;
				text-transform: none;
				font-weight: normal;
				font-style: normal;
				line-height: 1;
				width: 80px;
			}`;
			mysheet.insertRule(styles, 0);

			styles = `#vylda-clickable-area label input {
				position: relative;
				top: 2px;
				display: inline-block;
				margin: 0;
				padding: 0;
				font-size: 12px;
				text-transform: none;
				font-weight: normal;
				font-style: normal;
				line-height: 1;
			}`;
			mysheet.insertRule(styles, 0);

			styles = `.anchored-by-vylda {
				padding: 2px !important;
				background: rgba(123, 249, 155, 0.75) !important;
				border: 1px solid rgb(46, 58, 191) !important;
				line-height: 1 !important;
				cursor: pointer !important;
			}`;
			mysheet.insertRule(styles, 0);

			document.body.appendChild(dom.panel);
		}
	}

	const anchorsStart = () => {
		const anchors = document.querySelectorAll('a');
		console.info(`Clickable Area: nalezeno ${anchors.length} odkazů (${location.href}).`);
		Array.from(anchors).forEach((a) => {
			const cs = win.getComputedStyle(a);
			if (cs.display == 'inline') {
				a.style.display = 'inline-block';
			};
			a.classList[setup.anchors ? 'add' : 'remove'](ANCHOR_CLASS_NAME);
		});
	}

	buildMenu();

	if (setup.anchors) {
		anchorsStart();
	}

	if (setup.maps) {
		mapAreaStart();
	}
}())

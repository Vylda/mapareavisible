// ==UserScript==
// @name		Clickable Areas
// @namespace	http://gcgpx.cz/clickablaearea/
//@include		http*://*
// @version		2.1
// @description	Show clickable areas on image
// @updateURL	http://www.gcgpx.cz/clickablaearea/clickablaearea.meta.js
// @downloadURL http://www.gcgpx.cz/clickablaearea/clickablaearea.user.js
// @icon 		http://www.gcgpx.cz/clickablaearea/clickablaearea.png
// @icon64 		http://www.gcgpx.cz/clickablaearea/clickablaearea64.png
// @grant		none
// @license		GNU General Public License v2.0
// @copyright	2018-2050 Vilem Lipold (Vylda)
// @author		Vilem Lipold (Vylda)
// @run-at		document-idle
// ==/UserScript==

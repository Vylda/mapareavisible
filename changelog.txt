verze 2.1, 1. 1. 2024
	* Fix window
	* změna velikosti obrysu

verze 2.0, 25. 10. 2020
	* Fix klikacích map bez atributu shape
	* přepis do ES6

verze 1.11, 22. 3. 2019
	* Chrome na mapě nerespektuje border

verze 1.10, 21. 3. 2019
	* oprava různě velkých pímenek v názvu mapy (lze i prokliknout)
	* posun o border

verze 1.9, 21. 3. 2019
	* vykreslení mapy i při různě velkých písmenkách id, name a map

verze 1.8, 26. 2. 2019
	* ikonka se nestahuje
	* smazání nastavení
	* posun o border
	* poladění CSS

verze 1.7, 23. 2. 2019
	* drobné úpravy CSS

verze 1.6, 23. 2. 2019
	* přejmenování souborů

verze 1.5, 23. 2. 2019
	* zvýrazňování odkazů
	* menu
	* ukládání nastavení
	* metasoubor

verze 1.4, 23. 2. 2019
	* úprava skriptu pro verzování
	* lepší chování v GM
	* ikonka

verze 1.0, 13. 3. 2018
	* verze 1.0
